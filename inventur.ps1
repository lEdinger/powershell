cls
$Kundennummer = "2707";

$Xml = "C:\admin\"+$Kundennummer+".xml"

function Get-PcInfo($Computername = "localhost"){

if($Computername-eq (hostname)){$Computername = "localhost"}

$output =  Invoke-Command -ComputerName $Computername -Scriptblock {netsh firewall set service RemoteAdmin enable}

$Name = (Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).Name
$Hersteller = (Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).Manufacturer
$Modell = (Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).Model 
$OS = (Get-WmiObject Win32_OperatingSystem -computername $Computername).Caption + " " + (Get-WmiObject Win32_OperatingSystem -computername $Computername).OSArchitecture
$CPU = (Get-WmiObject Win32_Processor -ComputerName $Computername).Name
$RAM = "{0:n0}" -f ((Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).TotalPhysicalMemory /1GB)
$HDD = "{0:n0}" -f ((Get-WmiObject Win32_LogicalDisk -ComputerName $Computername | where DeviceID -like "*C*").Size[0] / 1073741824)
$IP = (Test-Connection $Computername -Count 1).IPV4Address.IPAddressToString
$WinKey = GetDepreciatedOS_PK $Computername

if($HDD -eq 0){ $HDD = "{0:n0}" -f (Invoke-Command -ComputerName $Computername -Scriptblock { (Get-Disk).size / 1GB} )}

$SN = (Get-WmiObject Win32_BIOS -ComputerName $Computername).SerialNumber

$myObject = New-Object -TypeName PSObject


                $myObject | Add-Member -type NoteProperty -name Name -Value $Name
                $myObject | Add-Member -type NoteProperty -name Hersteller -Value $Hersteller
                $myObject | Add-Member -type NoteProperty -name Modell -Value $Modell
                $myObject | Add-Member -type NoteProperty -name OS -Value $OS
                $myObject | Add-Member -type NoteProperty -name WinKey -Value $WinKey
                $myObject | Add-Member -type NoteProperty -name CPU -Value $CPU
                $myObject | Add-Member -type NoteProperty -name RAM -Value $RAM
                $myObject | Add-Member -type NoteProperty -name HDD -Value $HDD
                $myObject | Add-Member -type NoteProperty -name IP -Value $IP
                $myObject | Add-Member -type NoteProperty -name SN -Value $SN
                $myObject | Add-Member -type NoteProperty -name Server -Value 0
                $myObject | Add-Member -type NoteProperty -name Kundennummer -Value $Kundennummer
                
return $myObject

} # Gibt die Infos eines einzelnen PCs zurück.

function Add-ToXml($ComputerObject){
$ComputerListe = @()

$ComputerListe += Import-Clixml $Xml

$Vorhanden = 0

foreach($Computer in $ComputerListe){
if ($Computer.Name -eq $ComputerObject.Name){
$Vorhanden = 1}
}
if($Vorhanden -eq 0){
$ComputerListe += $ComputerObject

$ComputerListe | Export-Clixml $Xml}



}

Function Filter-Online{
<#
.Description
 
This is just a quick little script, but it does some cool things. Mainly I just created it in order to be able to quickly check whether computers are "online" or not.
 
It does this through a simple ping...so if a computer is On and Not responding to pings, it will not be listed as online.
 
This script can accept input either from the pipeline OR through the -parameter command
 
The great thing is that most things you would want to check their online status use the Name property so this works great for most things!
 
.Parameter Name
 
The only parameter that matters. Accepts just one or multiple objects
 
.Parameter NotOn
 
Used for if you would rather the returned objects be the ones that did Not respond to pings
.Example
 
get-adcomputer * |filter-online
 
Will take all of the computers listed in AD and return only the ones which are determined to be online
.Example
 
Filter-Online -name (get-vm)
Will take all of the computers listed in vCenter and return only the ones which are determined to be online. Yes, VMs have a PoweredOn Property, but that doesnt mean they have network connectivity
 
.Link
 www.vnoob.com
 
 .Notes
 
====================================================================
 Author:
 Conrad Ramos <conrad@vnoob.com> http://www.vnoob.com/
 
Date: 2012-3-14
 Revision: 1.0
 
====================================================================
#>
 
[CmdletBinding()]
param([Parameter(Mandatory=$True, ValueFromPipeline=$True)]$servers,[switch]$noton)
 
Begin{
$online=@()
$offline=@()
 
}
Process{
Foreach($server in $servers){
$reply=$null
 $ping = new-object System.Net.NetworkInformation.Ping
 $servername=$server.name
 Write-Verbose "Testing $servername"
try{
 $Reply = $ping.send($servername)
 }
Catch {}
if ($Reply.status -eq "Success")
{
$online+=$server}
else
{$offline+=$server}
}
 
}
 
End{
IF($noton -eq $false){
Write-Verbose "---------------------------------"
Write-Verbose "The following are online"
$online
}
Else{
Write-Verbose "---------------------------------"
Write-Verbose "The following are offline"
$offline
}
}
 
} # Filtert heraus welche PCs in der AD Online sind. get-adcomputer * |filter-online

function Get-ADClientList(){

return Get-ADComputer -Filter * | Filter-Online

} #Gibt eine Liste mit allen Clients in der AD aus und filtert bereits nicht vorhandene aus.

function GetDepreciatedOS_PK($Computername) {

if($Computername -eq "localhost"){



   $TrTable = "BCDFGHJKMPQRTVWXY2346789"

   $path    = "\SOFTWARE\Microsoft\Windows NT\CurrentVersion" 

   $IntArr  = ( get-itemproperty HKLM:\"$path" -Name 'DigitalProductId' ).DigitalProductId

   $isWin8  = (  ( ( $IntArr[ 66 ] -band 0xFF ) %  6 ) -band 1 )

   $IntArr[ 66 ] = ( $IntArr[ 66 ] -band 0xF7 ) -bor ( ( $isWin8 -band 2 ) * 4 )  

   $TrTable = "BCDFGHJKMPQRTVWXY2346789";  

   $PK      = "";  

   for ( $i = 24; $i -ge 0; $i-- ) 

   { 

      $r = 0; 

      for ( $j = 14; $j -ge 0; $j-- ) 

      { 

         $r = $r * 256;

         $r = $IntArr[ $j + 52 ] + $r;

         $IntArr[ $j + 52 ] =  [math]::Floor( [double]( $r / 24 ) ) 

         $r = $r % 24; 

      } 

      $PK = $TrTable[ $r ] + $PK;

   } 

   if ( $isWin8 -eq 1 )

   {

      $t1 = $PK.Substring( 1, $r );

      $PK = $PK.Substring( 1 ).replace( $t1, $t1 + 'N' );

      if ( $r -eq 0 ) { $PK = 'N' + $PK };

   }

   return "" + $PK.Substring(  0, 5 ) + "-" +$PK.Substring(  5, 5 ) + "-" +$PK.Substring( 10, 5 ) + "-" +$PK.Substring( 15, 5 ) + "-" +$PK.Substring( 20, 5 );





}else{

Invoke-Command -ComputerName $Computername -Scriptblock {

   $TrTable = "BCDFGHJKMPQRTVWXY2346789"

   $path    = "\SOFTWARE\Microsoft\Windows NT\CurrentVersion" 

   $IntArr  = ( get-itemproperty HKLM:\"$path" -Name 'DigitalProductId' ).DigitalProductId

   $isWin8  = (  ( ( $IntArr[ 66 ] -band 0xFF ) %  6 ) -band 1 )

   $IntArr[ 66 ] = ( $IntArr[ 66 ] -band 0xF7 ) -bor ( ( $isWin8 -band 2 ) * 4 )  

   $TrTable = "BCDFGHJKMPQRTVWXY2346789";  

   $PK      = "";  

   for ( $i = 24; $i -ge 0; $i-- ) 

   { 

      $r = 0; 

      for ( $j = 14; $j -ge 0; $j-- ) 

      { 

         $r = $r * 256;

         $r = $IntArr[ $j + 52 ] + $r;

         $IntArr[ $j + 52 ] =  [math]::Floor( [double]( $r / 24 ) ) 

         $r = $r % 24; 

      } 

      $PK = $TrTable[ $r ] + $PK;

   } 

   if ( $isWin8 -eq 1 )

   {

      $t1 = $PK.Substring( 1, $r );

      $PK = $PK.Substring( 1 ).replace( $t1, $t1 + 'N' );

      if ( $r -eq 0 ) { $PK = 'N' + $PK };

   }

   return "" + $PK.Substring(  0, 5 ) + "-" +$PK.Substring(  5, 5 ) + "-" +$PK.Substring( 10, 5 ) + "-" +$PK.Substring( 15, 5 ) + "-" +$PK.Substring( 20, 5 );



}}}

function Client-Inventur(){


$ComputerList = Get-ADComputer -Filter * -Property Name,OperatingSystem | select Name,OperatingSystem

$Clientlist = $ComputerList | where {($_.OperatingSystem -Like "*7*") -or ($_.OperatingSystem -Like "*10*") -or ($_.OperatingSystem -Like "* 8*")-or ($_.OperatingSystem -Like "*Vista*") }

$Computers = $Clientlist | Filter-Online

foreach ($Computer in $Computers) { 

$ComputerObject = Get-PcInfo $Computer.Name 

$ComputerObject.Server = 0

Add-ToXml $ComputerObject}

}

function Server-Inventur(){


$ComputerList = Get-ADComputer -Filter * -Property Name,OperatingSystem | select Name,OperatingSystem

$Clientlist = $ComputerList | where {($_.OperatingSystem -Like "*2008*") -or ($_.OperatingSystem -Like "*2012*") -or ($_.OperatingSystem -Like "*2016*")}

$Computers = $Clientlist | Filter-Online

foreach ($Computer in $Computers) { 

$ComputerObject = Get-PcInfo $Computer.Name 

$ComputerObject.Server = 1

Add-ToXml $ComputerObject}

}