﻿function Get-PcInfo($Computername = "localhost"){
# 
    if($Computername-eq (hostname)){$Computername = "localhost"}
# Enable Remote Admin
    $output =  Invoke-Command -ComputerName $Computername -Scriptblock {netsh firewall set service RemoteAdmin enable} -ErrorAction SilentlyContinue

# Gather Basic Information
    $Name = (Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).Name
    $Hersteller = (Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).Manufacturer
    $Modell = (Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).Model 
    $OS = (Get-WmiObject Win32_OperatingSystem -computername $Computername).Caption + " " + (Get-WmiObject Win32_OperatingSystem -computername $Computername).OSArchitecture
    $CPU = (Get-WmiObject Win32_Processor -ComputerName $Computername).Name   
    $RAM = "{0:n0}" -f ((Get-WmiObject -Class Win32_ComputerSystem -ComputerName $Computername).TotalPhysicalMemory /1GB)

# Gather Information about all HDDs
    # Create empty Array
        $HDD = @()
        $HDDs = Get-WmiObject Win32_LogicalDisk
    # Enumerate through all HDDs 
        foreach ($SingleHDD in $HDDs) {
        $tmpHDD = New-Object -TypeName PSObject
        $tmpHDD | Add-Member -type NoteProperty -name Size -value ("{0:n0}" -f ($SingleHDD.Size /1GB))
        $tmpHDD | Add-Member -type NoteProperty -name Letter -value ("{0:n0}" -f ($SingleHDD.Name))
        $tmpHDD | Add-Member -type NoteProperty -name Label -value $SingleHDD.VolumeName 
        $tmpHDD | Add-Member -type NoteProperty -name PercentFree -value ([math]::Round(($SingleHDD.FreeSpace / $SingleHDD.Size),2)*100)

        $HDD += $tmpHDD
        }
# Get ALL THE IPs! \o/
    $IP = (Get-NetIPConfiguration).IPv4Address.IPAddress             # OLD: (Test-Connection $Computername -Count 1).IPV4Address.IPAddressToString

# Get old Windows-Key
    $WinKey = Get-DepreciatedOS_PK $Computername

# Needed? Dunno
    #if($HDD -eq 0){ $HDD = "{0:n0}" -f (Invoke-Command -ComputerName $Computername -Scriptblock { (Get-Disk).size / 1GB} )}

#Get serial
    $SN = (Get-WmiObject Win32_BIOS -ComputerName $Computername).SerialNumber
# Create Custom Output-Object
    $myObject = New-Object -TypeName PSObject
    # Add the gathered information as NoteProperties
                    $myObject | Add-Member -type NoteProperty -name ComputerName -Value $Name
                    $myObject | Add-Member -type NoteProperty -name Hersteller -Value $Hersteller
                    $myObject | Add-Member -type NoteProperty -name Modell -Value $Modell
                    $myObject | Add-Member -type NoteProperty -name OS -Value $OS
                    $myObject | Add-Member -type NoteProperty -name WinKey -Value $WinKey
                    $myObject | Add-Member -type NoteProperty -name CPU -Value $CPU
                    $myObject | Add-Member -type NoteProperty -name RAM -Value $RAM
                    foreach ($SingleHDD in $HDD) {
                        $myObject | Add-Member -type NoteProperty -name ($SingleHDD.Letter + " Label") -Value $SingleHDD.Label
                        $myObject | Add-Member -type NoteProperty -name ($SingleHDD.Letter + " Size") -Value $SingleHDD.Size
                        $myObject | Add-Member -type NoteProperty -name ($SingleHDD.Letter + " PercentFree") -Value $SingleHDD.PercentFree
                        }
                    for ($k=1;$k -le $IP.Count; $k++) {
                        $myObject | Add-Member -type NoteProperty -name IP[$k] -Value $IP[$k-1]
                        }
                    $myObject | Add-Member -type NoteProperty -name SN -Value $SN
                    $myObject | Add-Member -type NoteProperty -name Server -Value 0
                    $myObject | Add-Member -type NoteProperty -name Kundennummer -Value $Kundennummer  #wird nicht festgelegt


# Make a XML out of it

[xml]$XMLDoc = New-Object System.Xml.XmlDocument
 
#create declaration
$XMLDeclaration = $XMLDoc.CreateXmlDeclaration("1.0","UTF-8",$null)
#append to document
$XMLDoc.AppendChild($XMLDeclaration) | Out-Null
 
#create a comment and append it in one line
#$text = @"
 
#Server Inventory Report
#Generated $(Get-Date)
#v1.0
 
#"@
 
#$XMLDoc.AppendChild($XMLdoc.CreateComment($text)) | Out-Null
 
#create root Node
$XMLroot = $XMLDoc.CreateNode("element","Computers",$null)
    $PC = $XMLdoc.CreateNode("element","Computer",$null)
    $PC.SetAttribute("Name",$Name)
     #create elements for the remaining properties
        "Hersteller", "Modell", "OS", "Winkey", "CPU", "RAM", "SN", "Kundennummer" | foreach {
        $TMPElement = $XMLDoc.CreateElement($_)
        $TMPElement.InnerText = (Get-Variable $_).Value
        $PC.AppendChild($TMPElement) | Out-Null
 }
 
 #add to parent node
        $HDDsNode = $XMLDoc.CreateNode("element", "HDDs", $null)        
            foreach ($SingleHDD in $HDD) {    
                $HDDNode = $XMLDoc.CreateNode("element", "HDD", $null)
                    "Letter", "Label", "Size" , "PercentFree" | Foreach {
                    $TMPElement = $XMLDoc.CreateElement($_)
                    $TMPElement.InnerText = ($SingleHDD.$_)
                    $HDDNode.AppendChild($TMPElement) | Out-Null   
                    
                    }
         <#       
                $LetterElement = $XMLDoc.CreateElement("Letter")
                $LetterElement.InnerText = $SingleHDD.Letter
                $HDDNode.AppendChild($LetterElement) | Out-Null
                $LabelElement = $XMLDoc.CreateElement("Label")
                $LabelElement.InnerText = $SingleHDD.Label
                $HDDNode.AppendChild($LabelElement) | Out-Null
                $SizeElement = $XMLDoc.CreateElement("Size")
                $SizeElement.InnerText = $SingleHDD.size
                $HDDNode.AppendChild($SizeElement) | Out-Null
                $PercentElement = $XMLDoc.CreateElement("PercentFree")
                $PercentElement.InnerText = $SingleHDD.PercentFree
                $HDDNode.AppendChild($PercentElement) | Out-Null #>
            $HDDsNode.AppendChild($HDDNode) | Out-Null  
            }
        $PC.AppendChild($HDDsNode) | Out-Null
        $IPNode = $XMLDoc.CreateNode("element", "IP-Adressen", $null)
        foreach ($SingleIP in $IP) {
            $IPElement = $XMLDoc.CreateElement("IP")
            $IPElement.InnerText = $SingleIP
            $IPNode.AppendChild($IPElement) | Out-Null

        $PC.AppendChild($IPNode) | Out-Null     
             }


 #append to root
 $XMLroot.AppendChild($PC) | Out-Null
 #foreach computer
 
#add root to the document
$XMLDoc.AppendChild($XMLroot) | Out-Null
return $XMLDoc

} # Gibt die Infos eines einzelnen PCs zurÃ¼ck.