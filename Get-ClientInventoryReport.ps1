﻿function Get-ClientInventoryReport(){


$ComputerList = Get-ADComputer -Filter * -Property Name,OperatingSystem | select Name,OperatingSystem

$Clientlist = $ComputerList | where {($_.OperatingSystem -Like "*7*") -or ($_.OperatingSystem -Like "*10*") -or ($_.OperatingSystem -Like "* 8*")-or ($_.OperatingSystem -Like "*Vista*") }

$Computers = $Clientlist | Filter-Online

foreach ($Computer in $Computers) { 

$ComputerObject = Get-PcInfo $Computer.Name 

$ComputerObject.Server = 0

Add-ToXml $ComputerObject}

}