﻿function Get-ServerInventoryReport(){


$ComputerList = Get-ADComputer -Filter * -Property Name,OperatingSystem | select Name,OperatingSystem

$Clientlist = $ComputerList | where {($_.OperatingSystem -Like "*2008*") -or ($_.OperatingSystem -Like "*2012*") -or ($_.OperatingSystem -Like "*2016*")}

$Computers = $Clientlist | Filter-Online

foreach ($Computer in $Computers) { 

$ComputerObject = Get-PcInfo $Computer.Name 

$ComputerObject.Server = 1

Add-ToXml $ComputerObject}

}