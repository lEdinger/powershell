﻿Function Filter-Online{
<#
.Description
 
This is just a quick little script, but it does some cool things. Mainly I just created it in order to be able to quickly check whether computers are "online" or not.
 
It does this through a simple ping...so if a computer is On and Not responding to pings, it will not be listed as online.
 
This script can accept input either from the pipeline OR through the -parameter command
 
The great thing is that most things you would want to check their online status use the Name property so this works great for most things!
 
.Parameter Name
 
The only parameter that matters. Accepts just one or multiple objects
 
.Parameter NotOn
 
Used for if you would rather the returned objects be the ones that did Not respond to pings
.Example
 
get-adcomputer * |filter-online
 
Will take all of the computers listed in AD and return only the ones which are determined to be online
.Example
 
Filter-Online -name (get-vm)
Will take all of the computers listed in vCenter and return only the ones which are determined to be online. Yes, VMs have a PoweredOn Property, but that doesnt mean they have network connectivity
 
.Link
 www.vnoob.com
 
 .Notes
 
====================================================================
 Author:
 Conrad Ramos <conrad@vnoob.com> http://www.vnoob.com/
 
Date: 2012-3-14
 Revision: 1.0
 
====================================================================
#>
 
[CmdletBinding()]
param([Parameter(Mandatory=$True, ValueFromPipeline=$True)]$servers,[switch]$noton)
 
Begin{
$online=@()
$offline=@()
 
}
Process{
Foreach($server in $servers){
$reply=$null
 $ping = new-object System.Net.NetworkInformation.Ping
 $servername=$server.name
 Write-Verbose "Testing $servername"
try{
 $Reply = $ping.send($servername)
 }
Catch {}
if ($Reply.status -eq "Success")
{
$online+=$server}
else
{$offline+=$server}
}
 
}
 
End{
IF($noton -eq $false){
Write-Verbose "---------------------------------"
Write-Verbose "The following are online"
$online
}
Else{
Write-Verbose "---------------------------------"
Write-Verbose "The following are offline"
$offline
}
}
 
} # Filtert heraus welche PCs in der AD Online sind. get-adcomputer * |filter-online